# Quoridor

Etapele care trebuiesc parcurse: 

- [x] crearea unui nou repository GIT
- [x] organizarea proiectului si structura fisierelor
- [x] structurarea proiectului pe clase si obiecte
- [x] organizarea proiectului si structura fisierului

To-do list:

- [x] creare tabla de joc
- [x] creare buton care afiseaza tabla de joc
- [x] adaugare optiuni de genul onHover, onClick etc
- [x] buton resetare totala -- tabla + onHover/click
- [x] input cu numele jucatorilor + afisare lor
