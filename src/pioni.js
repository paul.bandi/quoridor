//50

var xPion1 = 507,
    yPion1 = 100,
    xPion2 = 508,
    yPion2 = 500;

function pion1(x, y) {
    //pion1
    fill('#0AAD45');
    ellipse(x, y, 30, 30);
    fill('black');
    ellipse(x, y, 20, 20);
    fill("#0AAD45");
    ellipse(x, y, 12, 12);
}

function pion2(x, y) {
    //pion2
    fill('red');
    ellipse(x, y, 30, 30);
    fill('black');
    ellipse(x, y, 20, 20);
    fill('red');
    ellipse(x, y, 12, 12);
}