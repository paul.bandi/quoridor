var a = [
    [2, 2, 2, 2, 3, 2, 2, 2, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 1, 2],
    [2, 2, 2, 2, 4, 2, 2, 2, 2]
];

xUser = 507;
yUser = 500;

xComp = 507;
yComp = 100;
var ziduri1 = [2, 1, 1, 1, 1, 1, 1, 1, 1, 1];
var ziduri2 = [2, 1, 1, 1, 1, 1, 1, 1, 1, 1];

let button;

function setup() {
    bg = loadImage('images/202.jpg');
    createCanvas(800, 600);
    input = createInput();
    input.position(250, 60);

    button = createButton('Start Game');
    button.position(280, 85);
    button.mousePressed(greet);

    button = createButton('RESTART');
    button.position(240, 570);
    button.mousePressed(resetGame);
    button.size(140);

    greeting = createElement('h2', 'What is your name?');
    greeting.position(210, 5);
    greeting.style('color', 'white');
}

function resetGame(){
    xUser = 507;
    yUser = 500;

    xComp = 507;
    yComp = 100;
}

function greet() {
    const name = input.value();
    greeting.html('Hello ' + name + '!');
    input.value('');

}

function draw() {
    background(bg);
    insert();
    umbraZiduri();
}

function insert() {
    Table.matrice();
    Table.pozitieTabla();
    Table.ziduri();
    pion1(xComp, yComp);
    pion2(xUser, yUser);
}

function keyPressed() {
    let userPressedOnArrow = false;
    if (keyCode === LEFT_ARROW) {
        if (xUser - 50 > 300)
            {
                xUser -= 50;
                userPressedOnArrow = true;
            }

    } else if (keyCode === RIGHT_ARROW) {
        if (xUser + 50 < 710)
            {
                xUser += 50;
                userPressedOnArrow = true;
            }
    } else if (keyCode === UP_ARROW) {
        if (yUser - 50 > 99)
            {
                yUser -= 50;
                userPressedOnArrow = true;
            }

    } else if (keyCode === DOWN_ARROW) {
        if (yUser + 50 < 507)
            {
                yUser += 50;
                userPressedOnArrow = true;
            }

    }
    availableDirs = [38, 39, 37, 40];
    let computerChoice = availableDirs[Math.floor(Math.random() * 4)];
if(userPressedOnArrow){
     setTimeout(function () {
        if (computerChoice == 38) {
            if (yComp + 50 <= 500)
                yComp += 50;
        } else if (computerChoice == 37) {
            if (xComp - 50 >= 307)
                xComp -= 50;
        } else if (computerChoice == 40) {
            if (yComp - 50 >= 100)
                yComp -= 50;
        } else if (computerChoice == 39) {
            if (xComp + 50 <= 707)
                xComp += 50;
        }
    }, 400);
}
   



}
