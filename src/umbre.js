function umbraZiduri() {

   //umbra blocurilor pe tabla
   colorMode('hsb(60, 100%, 25%)');
   let c = color('#7d620a');
   fill(c);
   let x1, y1, x2, y2; //coordonate puncte pe orizontala
   let c1, c2, d1, d2; //coordonate puncte pe verticala
   let ok = false;


   // umbra ziduri pe verticala
   for (var j = 0; j < 8; j++) {
    for (var i = 0; i < 8; i++) {
       c1 = 220 + 40 * (j + 1) + 10 * j;
       c2 = 280 + 40 * (j + 1) + 10 * (j + 1);
       d1 = 80 + 40 * (i + 1) + 10 * 1;
       d2 = 80 + 40 * (i + 1) + 10 * (i + 1);
       if (!(mouseX < c1 && mouseX > c2 && mouseY < d1 && mouseY < d2)) {
          y1 = 80 + 40 * i + 10 * i + 20;
          y2 = 80 + 10 + 10 * (i + 1) + 40 * (i + 1);
          x1 = 280 + 40 * (j + 1) + 10 * j;
          x2 = 280 + 40 * (j + 1) + 10 * (j + 1);
          if (mouseX > x1 && mouseX < x2 && mouseY > y1 && mouseY < y2)
             rect(x2, y1-20, 10, 91);
       }
    }
 }
}
