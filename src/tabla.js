class Table {

    static matrice() {
        erase();
        rect(0, 0, 200, 600);
        noErase();

        // patrat rosu
        fill("#de0d0d");
        square(200, 0, 600);

        //blocuri negre mijloc
        fill('black');
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                if (a[i][j] != 0)
                    square(288 + i * 40 + i * 10, 80 + j * 40 + j * 10, 40);
            }
        }
    }

    static pozitieTabla() {

        //dungile gri
        fill("#999896");
        rect(270, 65, 460, 10);
        rect(270, 525, 460, 10);

        //marginile
        fill('black');
        for (let i = 0; i < 9; i++) {
            rect(280 + i * 40 + i * 10, 5, 40, 60);
            rect(280 + i * 40 + i * 10, 535, 50, 60);


        }

        //triunghiuri 
        triangle(205, 5, 270, 65, 270, 5);
        triangle(710, 5, 795, 5, 730, 64);
        triangle(205, 595, 270, 595, 270, 540);
        triangle(795, 595, 730, 595, 730, 540);
    }

    static ziduri() {

        //zidurile de sus
        fill('#fcba03');
        for (let i = 0; i < 10; i++)
            if (ziduri1[i] != 0) {
                rect(270 + i * 40 + i * 10, 0, 10, 65);
            }

        //zidurile de jos
        fill('#fcba03');
        for (let i = 0; i < 10; i++)
            if (ziduri2[i] != 0) {
                rect(270 + i * 40 + i * 10, 535, 10, 65);
            }
          
           }
 
        }
        